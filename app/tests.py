from django.test import TestCase, Client
from .views import story8
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import unittest

# Create your tests here.
class test_third(TestCase):
    def test_available(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_to_do_list_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'third.html')
